listClient1: listClient1.o list.o
	g++ -o listClient1 listClient1.o list.o

listClient1.o: listClient1.cpp list.h
	g++ -c listClient1.cpp

listClient1.o: listClient1.cpp list.h
	g++ -c listClient1.cpp

list.o: list.cpp list.h
	g++ -c list.cpp

clean:
	rm *.o listClient1
